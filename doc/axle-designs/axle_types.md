There are 3 main types of axles which are: Front, Rear, Stub axles.

Front Axle: Front axles are at the front of the vehicle, they hold load, allow movement and usually permit some type of steering
Rear Axle: Rear axles are located at the rear of the vehicle, they hold load, allow movement, and in some occasions permit steering
Stub Axle: Stub Axles are used for tranmitting motion on a higher degrees of freedom joint. They are usually mounted on the end of a front or rear axle

Types of Front Axles
    Live Axle:
    Dead Axle:

Types of Rear Axles
    Semi-Floating:
    Full Floating:
    3/4 Floating:

Types of Stub Axles
    Elliot:
    Reverse Elliot:
    Lamoine:
    Reversed Lamoine:

https://www.theengineerspost.com/types-of-axles/
https://www.caranddriver.com/research/a31547001/types-of-axle/
